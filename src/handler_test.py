import re
import os
import json

import requests_mock

from .handler import handle

# Test your handler here

# To disable testing, you can set the build_arg `TEST_ENABLED=false` on the CLI or in your stack.yml
# https://docs.openfaas.com/reference/yaml/#function-build-args-build-args


def test_handle():
    with open("test.json", "r") as fh:
        data = json.load(fh)
        assert len(data) > 10, "test.json length is less than 10, probably no good."

    with requests_mock.Mocker() as m:
        m.post(requests_mock.ANY, status_code=200, json=data)
        resp, code = handle(data, "test_user")
        assert resp.get_data(as_text=True), "no response."
        assert code == 200, "resp status code not 200."
