import os
import json
import requests
import structlog
from requests.auth import HTTPBasicAuth
from flask import Response

from .exceptions import APIError

log = structlog.stdlib.get_logger()


def handle(alert_data, current_user):
    grafantfy_token = os.environ.get("GRAFANTFY_TOKEN")
    ntfy_endpoint = os.environ.get("GRAFANTFY_URL", "https://ntfy.sh")
    ntfy_topic = os.environ.get("GRAFANTFY_TOPIC", "grafana")
    priorities = {
        "info": 1,
        "low": 2,
        "default": 3,
        "high": 4,
        "critical": 5,
    }

    if not alert_data and not json.dumps(alert_data):
        raise ValueError("request alert_data is not valid json")
    else:
        log.debug("grafana alert", **alert_data)

    try:
        if not grafantfy_token:
            with open("/var/openfaas/secrets/password") as fh:
                grafantfy_token = fh.read()

        basic = HTTPBasicAuth(current_user, grafantfy_token)
        resps = []
        data = {}

        data["topic"] = ntfy_topic
        data["title"] = alert_data.get("title", "")
        data["message"] = alert_data.get("message", "")

        for alert_item in alert_data.get("alerts", []):
            data["priority"] = priorities.get(
                alert_item.get("labels", {}).get("priority", "default").lower(), 3
            )
            data["click"] = alert_item.get("dashboardURL", "")
            data["actions"] = [
                {
                    "action": "view",
                    "label": "View Dashboard",
                    "url": alert_item.get("dashboardURL", ""),
                },
                {
                    "action": "view",
                    "label": "View Panel",
                    "url": alert_item.get("panelURL", ""),
                },
                {
                    "action": "view",
                    "label": "Silence",
                    "url": alert_item.get("silenceURL", ""),
                },
            ]

            data["tags"] = []
            for k, v in alert_item.get("labels", {}).items():
                data["tags"].append(f"{k}_{v}")

            log.debug("payload", **data)
            req = requests.post(
                f"{ntfy_endpoint}",
                json=data,
                headers={"Content-type": "application/json"},
                #auth=basic,
                timeout=5,
            )
            req.raise_for_status()
            resps.append(req.json())

        log.debug("ntfy api", responses=resps)
        return Response(json.dumps(resps), mimetype="application/json", status=200), 200
    except Exception as error:
        message = str(error)
        raise APIError("Unhandled exception", code=500, payload={}) from error
