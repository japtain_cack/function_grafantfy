[![Tag](https://img.shields.io/gitlab/v/tag/japtain_cack/function_grafantfy?style=for-the-badge)](https://gitlab.com/japtain_cack/function_grafantfy/-/tags)
[![Issues](https://img.shields.io/gitlab/issues/open/japtain_cack/function_grafantfy?style=for-the-badge)](https://gitlab.com/japtain_cack/function_grafantfy/-/issues)
[![Docker Stars](https://img.shields.io/docker/stars/nsnow/ip?style=for-the-badge)](https://hub.docker.com/r/nsnow/ip)
[![Docker Pulls](https://img.shields.io/docker/pulls/nsnow/ip?style=for-the-badge)](https://hub.docker.com/r/nsnow/ip)
[![License](https://img.shields.io/badge/License-CC%20BY--ND%204.0-blue?style=for-the-badge)](https://creativecommons.org/licenses/by-nd/4.0/)

# function_grafantfy

OpenFaaS Grafana -> ntfy integration Function.
