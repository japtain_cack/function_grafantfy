class APIError(Exception):
    """All custom API Exceptions"""

    code: int = 400
    description: str = "Bad request."

    def __init__(
        self,
        message: str,
        code: int = None,
        payload: dict = {},
    ):
        Exception.__init__(self)
        self.message: str = message
        if code is not None:
            self.code = code
        self.payload = payload
        self.code = self.code

    def to_dict(self) -> dict:
        rv: dict = dict(self.payload or ())
        rv["message"]: str = self.message
        rv["code"]: int = self.code
        return rv


class APIMalformedHeaders(APIError):
    """APIMalformedHeaders Error Class."""

    code: int = 400
    description: str = "Bad Request: Required header is missing."
