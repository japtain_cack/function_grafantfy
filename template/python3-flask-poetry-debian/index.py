# Copyright (c) Alex Ellis 2017. All rights reserved.
# Licensed under the MIT license. See LICENSE file in the project root for full license information.


from flask import request
from flask_httpauth import HTTPBasicAuth
from waitress import serve
import structlog
import os
import hashlib

from function import app
from function.handler import handle

logger = structlog.stdlib.get_logger()
logger.info(
    "logging initialized",
    log_level=os.environ.get("GRAFANTFY_LOG_LEVEL", "info").upper(),
)
auth = HTTPBasicAuth()


# distutils.util.strtobool() can throw an exception
def is_true(val):
    return len(val) > 0 and val.lower() == "true" or val == "1"


@auth.verify_password
def verify_password(username, token):
    grafantfy_token = os.environ.get("GRAFANTFY_TOKEN")
    if not token:
        return False

    if not grafantfy_token:
        with open("/var/openfaas/secrets/password", "r") as fh:
            grafantfy_token = fh.read()

    if token and grafantfy_token:
        grafantfy_token_hash = hashlib.sha512(grafantfy_token.encode("utf-8"))
        token_hash = hashlib.sha512(token.encode("utf-8"))
        if token_hash.digest() == grafantfy_token_hash.digest():
            return username


@app.route("/", defaults={"path": ""}, methods=["POST", "GET"])
@app.route("/<path:path>", methods=["POST", "GET"])
@auth.login_required
def main_route(path):
    raw_body = os.getenv("RAW_BODY", "false")
    as_text = True

    if is_true(raw_body):
        as_text = False

    ret = handle(request.get_json(silent=True), auth.current_user())
    return ret


if __name__ == "__main__":
    serve(app, host="0.0.0.0", port=5000)
